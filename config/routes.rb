Rails.application.routes.draw do
  resources :leaveforms
  post '/leaveforms/leavename', to: 'leaveforms#leaveName'
  resources :attendances
  post '/attendances/name', to: 'attendances#updateName'
	root 'homepages#home'
  get  '/home',   to: 'homepages#home'
  get  '/dashboard',   to: 'homepages#dashboard'
  get  '/signup',  to: 'employees#new'
  get 'sessions/new'
  resources :employees
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
