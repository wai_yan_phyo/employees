class Employee < ApplicationRecord
	 before_save { email.downcase! }
	 validates :firstname,  presence: true, length: { maximum: 50 }
	 validates :lastname,  presence: true, length: { maximum: 50 }
   validates :gender, presence:true, length: {maximum:6}
   validates :NRC, presence:true, length: {maximum:6}
   VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
   validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
   has_secure_password
   validates :password, presence: true, length: { minimum: 6 }

   has_many :attendances, class_name: "Attendance" , primary_key: "id", foreign_key: "employee"
   has_many :leaveforms, class_name: "Leaveform" , primary_key: "id", foreign_key: "employee"
end
