class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      user ||= User.new # guest user (not logged in)
      if user.user_roll == 'admin'
        can :manage, :all
      else
        can :read, Employee
        can :read, Attendance
        can :read, Leaveform
        can :create, Leaveform
      end
  end
end
