class Leaveform < ApplicationRecord
	validates :leave_type,  presence: true, length: { maximum: 50 }
	validates :start_date,  presence: true
	validates :end_date,  presence: true
  belongs_to :employee
end
