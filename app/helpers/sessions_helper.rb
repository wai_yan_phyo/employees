module SessionsHelper
	 # Logs in the given user.
  def log_in(employee)
    session[:employee_id] = employee.id
  end

  # Returns the current logged-in user (if any).
  def current_user
    if session[:employee_id]
      @current_user ||= Employee.find_by(id: session[:employee_id])
      # byebug
    end
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end

  # Logs out the current user.
  def log_out
    session.delete(:employee_id)
    @current_user = nil
  end

  def logged_in_employee
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
