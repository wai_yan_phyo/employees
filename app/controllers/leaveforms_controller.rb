class LeaveformsController < ApplicationController
  before_action :logged_in_employee, only: [:index,:show, :edit, :update, :destroy]
  before_action :set_leaveform, only: [:show, :edit, :update, :destroy]

  # GET /leaveforms
  # GET /leaveforms.json
  def index
    if current_user.user_roll == 'admin'
      @leaveforms = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname,employees.lastname")
    else
      @leaveforms = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname,employees.lastname").where(:employee_id => current_user.id)
    end
  end

  def about
  end

  # GET /leaveforms/1
  # GET /leaveforms/1.json
  def show
     @leaveform = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname,employees.lastname").find(params[:id])    
    if current_user
      authorize! :show, @leaveform
    end 
  end

  # GET /leaveforms/new
  def new
    @leaveform = Leaveform.new
  end

  # GET /leaveforms/1/edit
  def edit
  end

  # POST /leaveforms
  # POST /leaveforms.json
  def create
    @leaveform = Leaveform.new(leaveform_params)
    # @leaveform.start_date = DateTime.strptime(params[:leaveform][:start_date], "%m/%d/%Y").to_date
    # @leaveform.end_date = DateTime.strptime(params[:leaveform][:end_date], "%m/%d/%Y").to_date
    @isChecked = params[:leaveform][:Status]
    if  @isChecked == 'true'
      @leaveform.status = 'accept'
    else
      @leaveform.status = 'reject'
    end
    respond_to do |format|
      if @leaveform.save
        format.html { redirect_to @leaveform }
        format.json { render :show, status: :created, location: @leaveform }
      else
        format.html { render :new }
        format.json { render json: @leaveform.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leaveforms/1
  # PATCH/PUT /leaveforms/1.json
  def update
    # @leaveform = Leaveform.find(params[:id])


    
    respond_to do |format|
      # @leaveform.start_date = DateTime.strptime(params[:leaveform][:start_date], "%m/%d/%Y").to_date
      # byebug
      # @leaveform.end_date = DateTime.strptime(params[:leaveform][:end_date], "%m/%d/%Y").to_date
        
      if @leaveform.update(leaveform_params)
        # byebug 
        format.html { redirect_to @leaveform }
        format.json { render :show, status: :ok, location: @leaveform }
      else
        format.html { render :edit }
        format.json { render json: @leaveform.errors, status: :unprocessable_entity }
      end
    end
  end

  def leaveName
    @newemployee = Employee.find(params[:new_leaveform])
    puts @newemployee.firstname
    puts @newemployee.lastname
    render :json => { :success => "success" ,:firstname => @newemployee.firstname,:lastname => @newemployee.lastname}
  end

  # DELETE /leaveforms/1
  # DELETE /leaveforms/1.json
  def destroy
    @leaveform.destroy
    respond_to do |format|
      format.html { redirect_to leaveforms_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leaveform
      @leaveform = Leaveform.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def leaveform_params
      params.require(:leaveform).permit(:employee_id, :leave_type, :start_date, :end_date, :reason)
    end
end
