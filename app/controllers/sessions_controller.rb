class SessionsController < ApplicationController
  def new 
  end

  def create
    employee = Employee.find_by(email: params[:session][:email].downcase)
    # byebug
    if employee && employee.authenticate(params[:session][:password])
      # Log the employee in and redirect to the employee's show page.
    	# byebug
      log_in employee
      redirect_to employee 
    else
      # Create an error message.
      flash.now[:danger] = "Email or password is invalid"
      render "new"
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
