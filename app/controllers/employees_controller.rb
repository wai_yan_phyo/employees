class EmployeesController < ApplicationController
  before_action :logged_in_employee, only: [:index,:show, :edit, :update, :destroy]
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.paginate(page: params[:page],per_page: 8)
    authorize! :index, @employees
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
      @employees = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname,employees.lastname").where(:employee_id => params[:id]) 
  end

  # GET /employees/new
  def new
    @employee = Employee.new
    authorize! :new, @employees
  end

  # GET /employees/1/edit
  def edit
     @employee = Employee.find(params[:id])
     authorize! :edit, @employees
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)
    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    authorize! :destroy, @employees
    respond_to do |format|
      format.html { redirect_to employees_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:firstname, :lastname,:user_roll, :phone_no, :email, :password, :address, :gender, :NRC, :DateofBirth, :Join_Date, :Terminated_Date, :Position, :Salary)
    end
end
