class HomepagesController < ApplicationController
  def home
  end

  def dashboard
  	if current_user.user_roll == 'admin'
  		@leaveforms = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname,employees.lastname")
  	else
  		@leaveforms = Leaveform.joins(:employee).select("leaveforms.*,employees.firstname,employees.lastname").where(:employee_id => current_user.id)
  	end
 
  	if current_user.user_roll == 'admin'
      @attendances = Attendance.joins(:employee).select("attendances.*,employees.firstname,employees.lastname")
    else
      @attendances = Attendance.joins(:employee).select("attendances.*,employees.firstname,employees.lastname").where(:employee_id => current_user.id)
    end
	end

end
