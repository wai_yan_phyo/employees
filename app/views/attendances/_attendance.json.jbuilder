json.extract! attendance, :id, :employee_id, :start_time, :end_time, :attendance_date, :remark, :created_at, :updated_at
json.url attendance_url(attendance, format: :json)
