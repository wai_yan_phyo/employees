json.extract! employee, :id, :firstname, :lastname, :phone_no, :email, :password, :address, :gender, :NRC, :DateofBirth, :Join_Date, :Terminated_Date, :Position, :Salary, :created_at, :updated_at
json.url employee_url(employee, format: :json)
