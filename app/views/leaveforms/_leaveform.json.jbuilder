json.extract! leaveform, :id, :employee_id, :leave_type, :start_date, :end_date, :reason, :created_at, :updated_at
json.url leaveform_url(leaveform, format: :json)
