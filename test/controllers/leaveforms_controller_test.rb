require 'test_helper'

class LeaveformsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @leaveform = leaveforms(:one)
  end

  test "should get index" do
    get leaveforms_url
    assert_response :success
  end

  test "should get new" do
    get new_leaveform_url
    assert_response :success
  end

  test "should create leaveform" do
    assert_difference('Leaveform.count') do
      post leaveforms_url, params: { leaveform: { employee_id: @leaveform.employee_id, end_date: @leaveform.end_date, leave_type: @leaveform.leave_type, reason: @leaveform.reason, start_date: @leaveform.start_date } }
    end

    assert_redirected_to leaveform_url(Leaveform.last)
  end

  test "should show leaveform" do
    get leaveform_url(@leaveform)
    assert_response :success
  end

  test "should get edit" do
    get edit_leaveform_url(@leaveform)
    assert_response :success
  end

  test "should update leaveform" do
    patch leaveform_url(@leaveform), params: { leaveform: { employee_id: @leaveform.employee_id, end_date: @leaveform.end_date, leave_type: @leaveform.leave_type, reason: @leaveform.reason, start_date: @leaveform.start_date } }
    assert_redirected_to leaveform_url(@leaveform)
  end

  test "should destroy leaveform" do
    assert_difference('Leaveform.count', -1) do
      delete leaveform_url(@leaveform)
    end

    assert_redirected_to leaveforms_url
  end
end
