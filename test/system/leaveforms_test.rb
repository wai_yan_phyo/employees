require "application_system_test_case"

class LeaveformsTest < ApplicationSystemTestCase
  setup do
    @leaveform = leaveforms(:one)
  end

  test "visiting the index" do
    visit leaveforms_url
    assert_selector "h1", text: "Leaveforms"
  end

  test "creating a Leaveform" do
    visit leaveforms_url
    click_on "New Leaveform"

    fill_in "Employee", with: @leaveform.employee_id
    fill_in "End date", with: @leaveform.end_date
    fill_in "Leave type", with: @leaveform.leave_type
    fill_in "Reason", with: @leaveform.reason
    fill_in "Start date", with: @leaveform.start_date
    click_on "Create Leaveform"

    assert_text "Leaveform was successfully created"
    click_on "Back"
  end

  test "updating a Leaveform" do
    visit leaveforms_url
    click_on "Edit", match: :first

    fill_in "Employee", with: @leaveform.employee_id
    fill_in "End date", with: @leaveform.end_date
    fill_in "Leave type", with: @leaveform.leave_type
    fill_in "Reason", with: @leaveform.reason
    fill_in "Start date", with: @leaveform.start_date
    click_on "Update Leaveform"

    assert_text "Leaveform was successfully updated"
    click_on "Back"
  end

  test "destroying a Leaveform" do
    visit leaveforms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Leaveform was successfully destroyed"
  end
end
