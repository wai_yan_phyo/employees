require "application_system_test_case"

class EmployeesTest < ApplicationSystemTestCase
  setup do
    @employee = employees(:one)
  end

  test "visiting the index" do
    visit employees_url
    assert_selector "h1", text: "Employees"
  end

  test "creating a Employee" do
    visit employees_url
    click_on "New Employee"

    fill_in "Dateofbirth", with: @employee.DateofBirth
    fill_in "Join date", with: @employee.Join_Date
    fill_in "Nrc", with: @employee.NRC
    fill_in "Position", with: @employee.Position
    fill_in "Salary", with: @employee.Salary
    fill_in "Terminated date", with: @employee.Terminated_Date
    fill_in "Address", with: @employee.address
    fill_in "Email", with: @employee.email
    fill_in "Firstname", with: @employee.firstname
    fill_in "Gender", with: @employee.gender
    fill_in "Lastname", with: @employee.lastname
    fill_in "Phone no", with: @employee.phone_no
    click_on "Create Employee"

    assert_text "Employee was successfully created"
    click_on "Back"
  end

  test "updating a Employee" do
    visit employees_url
    click_on "Edit", match: :first

    fill_in "Dateofbirth", with: @employee.DateofBirth
    fill_in "Join date", with: @employee.Join_Date
    fill_in "Nrc", with: @employee.NRC
    fill_in "Position", with: @employee.Position
    fill_in "Salary", with: @employee.Salary
    fill_in "Terminated date", with: @employee.Terminated_Date
    fill_in "Address", with: @employee.address
    fill_in "Email", with: @employee.email
    fill_in "Firstname", with: @employee.firstname
    fill_in "Gender", with: @employee.gender
    fill_in "Lastname", with: @employee.lastname
    fill_in "Phone no", with: @employee.phone_no
    click_on "Update Employee"

    assert_text "Employee was successfully updated"
    click_on "Back"
  end

  test "destroying a Employee" do
    visit employees_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Employee was successfully destroyed"
  end
end
