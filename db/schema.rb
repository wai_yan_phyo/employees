# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_24_065255) do

  create_table "attendances", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "employee_id"
    t.time "start_time"
    t.time "end_time"
    t.datetime "attendance_date"
    t.string "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_id", "created_at"], name: "index_attendances_on_employee_id_and_created_at"
    t.index ["employee_id"], name: "index_attendances_on_employee_id"
  end

  create_table "employees", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.integer "phone_no"
    t.string "email"
    t.string "address"
    t.string "gender"
    t.string "NRC"
    t.datetime "DateofBirth"
    t.datetime "Join_Date"
    t.datetime "Terminated_Date"
    t.string "Position"
    t.integer "Salary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "user_roll"
    t.index ["email"], name: "index_employees_on_email", unique: true
  end

  create_table "leaveforms", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "employee_id"
    t.string "leave_type"
    t.date "start_date"
    t.date "end_date"
    t.string "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.index ["employee_id", "created_at"], name: "index_leaveforms_on_employee_id_and_created_at"
    t.index ["employee_id"], name: "index_leaveforms_on_employee_id"
  end

  add_foreign_key "attendances", "employees"
  add_foreign_key "leaveforms", "employees"
end
