# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Employee.create!(firstname:  "Example",
						 lastname:  "User",
						 phone_no:'123456',
             email: "admin@gmail.com",
             address:"yangon",
             password:"123456",
             gender:"male",
             NRC:"123456",
             Join_Date:"2.12.2016",
             Terminated_Date:  "2.12.2018",
             Position: "Senior Developer",
             Salary:"500000",
             user_roll:"admin")	


50.times do |n|
  firstname  = Faker::Name.name
  lastname  = Faker::Name.name
  phone_no='123456'
  email = "user-#{n+1}@railstutorial.org"
  gender="male"
  password = "password"
  address="yangon"
  NRC="123456"
  Join_Date='2.12.2016'
  Position="Junior Developer"
  Salary='200000'
  user_roll='user'
  Employee.create!(firstname:  firstname,
										lastname:    lastname,
										phone_no:phone_no,
										email:       email,
										address:     address,
				            password:    password,
										gender:      gender,
				            NRC:         NRC,
				            Join_Date:  Join_Date,
				            Position:Position,
				            Salary:Salary,
				            user_roll:user_roll)
end