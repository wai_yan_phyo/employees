class CreateAttendances < ActiveRecord::Migration[5.2]
  def change
    create_table :attendances do |t|
      t.references :employee, foreign_key: true
      t.time :start_time
      t.time :end_time
      t.datetime :attendance_date
      t.string :remark

      t.timestamps
    end
    add_index :attendances, [:employee_id, :created_at]
  end
end
