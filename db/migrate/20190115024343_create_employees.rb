class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :firstname
      t.string :lastname
      t.integer :phone_no
      t.string :email
      t.string :address
      t.string :gender
      t.string :NRC
      t.datetime :DateofBirth
      t.datetime :Join_Date
      t.datetime :Terminated_Date
      t.string :Position
      t.integer :Salary

      t.timestamps
    end
  end
end
