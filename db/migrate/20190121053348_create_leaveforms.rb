class CreateLeaveforms < ActiveRecord::Migration[5.2]
  def change
    create_table :leaveforms do |t|
      t.references :employee, foreign_key: true
      t.string :leave_type
      t.date :start_date
      t.date :end_date
      t.string :reason

      t.timestamps
    end
    add_index :leaveforms, [:employee_id, :created_at]
  end
end
