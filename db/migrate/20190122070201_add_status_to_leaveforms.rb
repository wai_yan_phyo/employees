class AddStatusToLeaveforms < ActiveRecord::Migration[5.2]
  def change
    add_column :leaveforms, :status, :string
  end
end
